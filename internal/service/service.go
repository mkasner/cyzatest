package service

import (
	"log"
	"net/http"

	"github.com/mkasner/cyza/internal/cyza"
	"github.com/mkasner/cyza/internal/server"
	"github.com/mkasner/cyza/internal/session"
	"github.com/mkasner/cyza/internal/store"

	"time"
)

type Service struct {
	store  store.Store
	addr   string
	server *server.Server
}

func New(addr string, store store.Store, tmplPath string, sessions session.Store, mail string, port int, username, password string, lclient, lsecret string) (*Service, error) {
	t := &Service{
		addr:  addr,
		store: store,
	}

	cyz := cyza.New(t.store, tmplPath, mail, port, username, password, addr)

	var err error
	t.server, err = server.New(cyz, tmplPath, sessions, lclient, lsecret)
	if err != nil {
		return t, err
	}

	return t, nil
}

func (t *Service) Go(stop, done chan struct{}) {
	go func() {
		go func() {
			log.Println("http service started: ", t.addr)
			err := http.ListenAndServe(t.addr, nil)
			if err != nil {
				log.Println("Error starting or stopping http server: ", err)
			}
		}()
		select {
		case <-stop:
			log.Println("closing services")
			time.Sleep(100 * time.Millisecond)
			close(done)
		}
	}()
}
