package cyza

import (
	"github.com/mkasner/cyza/internal/model"
	"github.com/mkasner/cyza/internal/store"
	"golang.org/x/net/context"
)

type Cyza struct {
	ctx      context.Context
	Store    store.Store
	ms       model.MailSettings
	tmplPath string
	Addr     string
}

func New(store store.Store, tmplPath string, mail string, port int, username, password string, addr string) *Cyza {
	c := &Cyza{
		ctx:      context.Background(),
		Store:    store,
		ms:       model.NewMailSettings(mail, port, username, password),
		tmplPath: tmplPath,
		Addr:     addr,
	}
	return c
}
