package cyza

import (
	"bytes"
	"crypto/rand"
	"errors"
	"fmt"
	"html/template"
	"io"
	"log"
	"net"
	"net/smtp"
	"path/filepath"
	"strconv"
	"time"

	"github.com/mkasner/cyza/internal/model"
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/net/context"
)

const (
	PASSLEN                              = 8
	recoverPasswordExpires time.Duration = 6 * time.Hour
	MARKER                               = "MAILEMAILBOUNDARY"
)

func (t *Cyza) CreateUser(ctx context.Context, user model.User, password, repeatpassword string) (model.User, error) {

	existingUser, err := t.Store.User(ctx, user.Username, "")
	if err == nil && len(existingUser.Id) > 0 {
		return user, errors.New("User already exists")
	}
	passwordHash := generatePasswordHash(password)

	user, err = t.Store.SaveUser(ctx, user, passwordHash)
	if err != nil {
		return user, err
	}
	return user, nil
}

func (t *Cyza) UpdateUser(ctx context.Context, user model.User) (model.User, error) {
	return t.Store.SaveUser(ctx, user, "")
}

func (t *Cyza) ForgotPassword(ctx context.Context, user model.User) error {
	token, err := t.CreateRecoverPassword(ctx, user.Id, user.Email)
	if err != nil {
		err = fmt.Errorf("Error saving recover password token: %v", err)
		log.Println(err)
		return err
	}
	//// send forgot password email
	tpl, err := template.ParseFiles(filepath.Join(t.tmplPath, "email/forgotpassword.html"))
	if err != nil {
		return err
	}
	buff := bytes.NewBuffer(nil)
	host := GetHost("", t.Addr)
	err = tpl.Execute(buff, struct {
		RecoverUrl string
		Host       string
	}{
		RecoverUrl: fmt.Sprintf("%s/recoverpassword?t=%s", host, token),
		Host:       host,
	})
	if err != nil {
		return err
	}
	err = t.sendEmail("mislav.kasner@gmail.com", user.Email, "Cyza forgot password", buff.Bytes())
	if err != nil {
		err = fmt.Errorf("Error sending email for recover password %s: %v", token, err)
		return err
	}
	//return err
	return nil
}
func (t *Cyza) Authenticate(ctx context.Context, username, password string) error {

	user, err := t.Store.User(ctx, username, "")
	if err != nil {
		log.Println(err)
		return ErrUserPasswordFail
	}
	switch user.Status {
	case model.UserStatusActive, model.UserStatusPartProfile:
		// ok users, continue
	default:
		log.Println("Wrong status", user.Status)
		return ErrUserPasswordFail
	}
	passwordFromDb, err := t.Store.Password(ctx, user.Id)
	if err != nil {
		log.Println("Cannot find password", err)
		return ErrUserPasswordFail
	}
	fmt.Println("Password:", passwordFromDb)
	err = bcrypt.CompareHashAndPassword([]byte(passwordFromDb), []byte(password))
	if err != nil {
		log.Println("Error comparing password hash", err)
		return ErrUserPasswordFail
	}
	return nil
}

func (t *Cyza) ChangePassword(ctx context.Context, user model.User, password string) error {
	var err error
	fmt.Println("Changing password:", user)
	user, err = t.Store.User(ctx, "", user.Id)
	if err != nil {
		return err
	}
	fmt.Println("Got user:", user)
	passwordHash := generatePasswordHash(password)

	return t.Store.ChangePassword(ctx, user.Id, passwordHash)
}

func generatePasswordHash(password string) string {
	hash, _ := bcrypt.GenerateFromPassword([]byte(password), 0)
	return string(hash)
}

func (t *Cyza) RandomPassword() string {
	return randString(PASSLEN)
}
func (t *Cyza) ValidatePassword(pass, passConfirm string) error {
	if len(pass) < 6 {
		return errors.New("Password too short. Min length: 6")
	}
	if pass != passConfirm {
		err := errors.New("Passwords don't match")
		return err
	}
	return nil
}
func (t *Cyza) CreateRecoverPassword(ctx context.Context, user string, email string) (string, error) {
	token := randString(32)
	expires := time.Now().Add(recoverPasswordExpires)
	rp := model.RecoverPassword{
		Token:   token,
		Email:   email,
		UserId:  user,
		Status:  model.RpStatusSent,
		Expires: expires,
	}
	err := t.Store.SaveRecoverPassword(ctx, rp)
	return rp.Token, err
}
func (t *Cyza) UserRecoverPassword(ctx context.Context, token string) (model.User, error) {
	var result model.User
	rp, err := t.Store.UserRecoverPassword(ctx, token)
	if err != nil {
		return result, err
	}
	fmt.Println(rp)
	if rp.Status != model.RpStatusSent {
		return result, errors.New("Token expired")
	}
	result.Id = rp.UserId
	return result, nil

}
func (t *Cyza) UpdateRecoverPassword(ctx context.Context, token string, status model.RecoverPasswordStatus) error {
	rp, err := t.Store.UserRecoverPassword(ctx, token)
	if err != nil {
		return err
	}
	rp.Status = status
	err = t.Store.SaveRecoverPassword(ctx, rp)
	return err
}
func GetHost(scheme, addr string) string {
	var result string
	if len(scheme) == 0 {
		scheme = "http"
	}
	host, port, _ := net.SplitHostPort(addr)
	if len(host) == 0 {
		host = "localhost"
	}
	if port == "443" {
		scheme = "https"
	}
	result = fmt.Sprintf("%s://%s", scheme, host)
	if len(port) > 0 && port != "80" && port != "443" {
		result += ":" + port
	}
	return result
}

func (t *Cyza) sendEmail(from, to, subject string, body []byte) error {
	buff := bytes.NewBuffer(nil)
	buff.WriteString(fmt.Sprintf(emailTpl(), from, to, "", subject)) // headers
	buff.Write(body)                                                 // body
	msg := buff.Bytes()
	auth := smtp.PlainAuth("", t.ms.Username, t.ms.Password, t.ms.Server)
	err := smtp.SendMail(t.ms.Server+":"+strconv.Itoa(t.ms.Port), auth, t.ms.Username, []string{to}, msg)
	return err

}
func emailTpl() string {
	return "From: %s \r\nTo: %s \r\nCc: %s \r\nSubject: %s \r\nMIME-version: 1.0 \r\nContent-Type: text/html; charset=\"UTF-8\"\r\n"
}
func randString(length int) string {
	buff := make([]byte, length/2)
	io.ReadFull(rand.Reader, buff)
	return fmt.Sprintf("%x", buff)
}
