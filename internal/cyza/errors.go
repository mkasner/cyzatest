package cyza

import "errors"

var (
	ErrUserPasswordFail = errors.New("Username or password not correct")
)
