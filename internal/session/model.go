package session

import "time"

type Session struct {
	Id        string
	Username  string
	UserId    string
	UpdatedAt time.Time
	LCode     string
	LState    string
}
