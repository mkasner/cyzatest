package inmem

import (
	"errors"
	"sync"

	"github.com/mkasner/cyza/internal/session"
)

type InmemSessionStore struct {
	lock  sync.RWMutex
	store map[string]*session.Session
}

func NewInmemSessionStore() (session.Store, error) {
	bss := &InmemSessionStore{
		store: make(map[string]*session.Session),
	}
	return bss, nil
}

// Gets process from store
func (t *InmemSessionStore) Session(sessionId string) (*session.Session, error) {
	t.lock.RLock()
	defer t.lock.RUnlock()
	if session, ok := t.store[sessionId]; ok {
		return session, nil
	}
	return nil, errors.New("Session not found")
}

func (t *InmemSessionStore) SaveSession(sess *session.Session) error {
	t.lock.Lock()
	t.store[sess.Id] = sess
	t.lock.Unlock()
	return nil
}

func (t *InmemSessionStore) DeleteSession(sessionId string) error {

	t.lock.Lock()
	delete(t.store, sessionId)
	t.lock.Unlock()
	return nil
}

func (t *InmemSessionStore) Sessions() ([]session.Session, error) {
	var result []session.Session
	t.lock.RLock()
	defer t.lock.RUnlock()
	for _, s := range t.store {
		result = append(result, *s)
	}
	return result, nil
}

func (t *InmemSessionStore) Close() {
}
