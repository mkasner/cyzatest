package session

import (
	"fmt"
	"log"
	"time"
)

type SessionCleanupService struct {
	sessionStore Store
	interval     time.Duration
	maxAge       time.Duration
}

func NewSessionCleanupService(maxAge time.Duration, ss Store) *SessionCleanupService {
	scs := &SessionCleanupService{
		sessionStore: ss,
		interval:     6 * time.Hour, // 6 hours
		maxAge:       maxAge,
	}

	return scs
}

func (t *SessionCleanupService) cleanup() {
	sessions, err := t.sessionStore.Sessions()
	if err != nil {
		log.Println(err)
	}
	var count int
	for _, s := range sessions {
		if t.shouldDelete(s) {
			err := t.sessionStore.DeleteSession(s.Id)
			if err != nil {
				log.Println(err)
				continue
			}
			count++

		}
	}

}

func (t *SessionCleanupService) shouldDelete(session Session) bool {
	if time.Since(session.UpdatedAt) >= t.maxAge {
		return true
	}
	return false
}

func (t *SessionCleanupService) Go(stop, done chan struct{}) {
	go func() {
		log.Println("starting session cleanup")
		t.cleanup()
		timer := time.NewTimer(t.interval)
		for {
			select {
			case <-stop:
				log.Println("closing session cleanup")
				time.Sleep(100 * time.Millisecond)
				close(done)
				return
			case <-timer.C:
				t.cleanup()
			}
		}
	}()
}

func (t *SessionCleanupService) Close() {
	fmt.Println("closing session cleanup")
}
