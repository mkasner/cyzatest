package store

import (
	"errors"
	"fmt"

	"github.com/mkasner/cyza/internal/model"
	"golang.org/x/net/context"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	cUser            = "user"
	cPassword        = "password"
	cRecoverPassword = "recover_password"
)

type MongoStore struct {
	session *mgo.Session
	db      *mgo.Database
}

func NewMongoStore(url string) (Store, error) {
	s := &MongoStore{}
	var err error
	s.session, err = mgo.Dial(url)
	s.db = s.session.DB("cyza")
	return s, err
}

func (t *MongoStore) User(ctx context.Context, username string, id string) (model.User, error) {
	var user model.User
	if len(username) > 0 {
		err := t.db.C(cUser).Find(bson.M{"username": username}).One(&user)
		return user, err
	}
	if len(id) > 0 {
		err := t.db.C(cUser).FindId(id).One(&user)
		return user, err
	}
	return user, errors.New("No params provided")
}

func (t *MongoStore) SaveUser(ctx context.Context, user model.User, password string) (model.User, error) {
	if len(user.Id) == 0 {
		user.Id = bson.NewObjectId().Hex()
		err := t.db.C(cUser).Insert(user)
		if err != nil {
			return user, err
		}
		p := model.Password{Id: user.Id, Password: password}
		err = t.db.C(cPassword).Insert(p)
		return user, err
	} else {
		err := t.db.C(cUser).UpdateId(user.Id, user)
		return user, err
	}
}

func (t *MongoStore) ChangePassword(ctx context.Context, id string, password string) error {
	_, err := t.User(ctx, "", id)
	if err != nil {
		return err
	}
	p := model.Password{Id: id, Password: password}
	err = t.db.C(cPassword).UpdateId(p.Id, p)
	return err
}
func (t *MongoStore) Password(ctx context.Context, id string) (string, error) {
	fmt.Println("Searching for password: ", id)
	var p model.Password
	err := t.db.C(cPassword).FindId(id).One(&p)
	return p.Password, err
}
func (t *MongoStore) UpdateUserStatus(ctx context.Context, id string, status model.UserStatus) error {
	user, err := t.User(ctx, "", id)
	if err != nil {
		return err
	}
	user.Status = status
	err = t.db.C(cUser).UpdateId(user.Id, user)
	return err
}

func (t *MongoStore) Close() error {
	t.session.Close()
	return nil
}
func (t *MongoStore) UserRecoverPassword(ctx context.Context, token string) (model.RecoverPassword, error) {
	var rp model.RecoverPassword
	err := t.db.C(cRecoverPassword).Find(bson.M{"token": token}).One(&rp)
	return rp, err
}
func (t *MongoStore) SaveRecoverPassword(ctx context.Context, rp model.RecoverPassword) error {
	_, err := t.db.C(cRecoverPassword).Upsert(bson.M{"token": rp.Token}, rp)
	return err
}
