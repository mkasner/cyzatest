package store

import (
	"crypto/rand"
	"fmt"
	"io"
	"testing"
	"time"

	"github.com/mkasner/cyza/internal/model"
	"golang.org/x/net/context"
)

const murl = "localhost:27017"

func TestCreateUser(t *testing.T) {
	s, ctx := setup(t)
	user := model.User{
		Username: "mislav.kasner@gmail.com",
		Name:     "Mislav Kasner",
		Created:  time.Now(),
		Status:   model.UserStatusPartProfile,
	}
	password := "paasss"
	user, err := s.SaveUser(ctx, user, password)
	if err != nil {
		t.Fatal(err)
	}
	userFromDb, err := s.User(ctx, "", user.Id)
	if err != nil {
		t.Fatal(err)
	}
	if userFromDb.Username != user.Username {
		t.Errorf("Username not saved correctly. Expected: %s, Got: %s", user.Username, userFromDb.Username)
	}
	fmt.Println(userFromDb)
}

func TestFullUser(t *testing.T) {
	s, ctx := setup(t)
	user := model.User{
		Username: "mislav.kasner@gmail.com",
		Name:     "Mislav Kasner",
		Created:  time.Now(),
		Status:   model.UserStatusPartProfile,
	}
	password := "paasss"
	user, err := s.SaveUser(ctx, user, password)
	if err != nil {
		t.Fatal(err)
	}
	userFromDb, err := s.User(ctx, "", user.Id)
	if err != nil {
		t.Fatal(err)
	}
	userFromDb.Address = model.Address{
		Street:  "Repusnica, Lipa 91",
		City:    "Kutina",
		State:   "HR",
		Country: "Croatia",
	}
	userFromDb.Status = model.UserStatusActive
	userFromDb, err = s.SaveUser(ctx, userFromDb, "")
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(userFromDb)
}
func TestRecoverPassword(t *testing.T) {
	s, ctx := setup(t)
	user := model.User{
		Username: "mislav.kasner@gmail.com",
		Email:    "mislav.kasner@gmail.com",
		Name:     "Mislav Kasner",
		Created:  time.Now(),
		Status:   model.UserStatusPartProfile,
	}
	password := "paasss"
	user, err := s.SaveUser(ctx, user, password)
	if err != nil {
		t.Fatal(err)
	}
	rp := model.RecoverPassword{
		Token:   randString(32),
		Email:   user.Email,
		UserId:  user.Id,
		Expires: time.Now(),
		Status:  model.RpStatusNew,
	}
	err = s.SaveRecoverPassword(ctx, rp)
	if err != nil {
		t.Fatal(err)
	}

}

func setup(t *testing.T) (Store, context.Context) {
	s, err := NewMongoStore(murl)
	if err != nil {
		t.Fatal(err)
	}
	return s, context.Background()
}
func randString(length int) string {
	buff := make([]byte, length/2)
	io.ReadFull(rand.Reader, buff)
	return fmt.Sprintf("%x", buff)
}
