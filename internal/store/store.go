package store

import (
	"github.com/mkasner/cyza/internal/model"
	"golang.org/x/net/context"
)

type Store interface {
	//auth
	User(context.Context, string, string) (model.User, error)
	SaveUser(context.Context, model.User, string) (model.User, error)
	ChangePassword(context.Context, string, string) error
	Password(context.Context, string) (string, error)
	UpdateUserStatus(context.Context, string, model.UserStatus) error
	// recover password
	UserRecoverPassword(context.Context, string) (model.RecoverPassword, error)
	SaveRecoverPassword(context.Context, model.RecoverPassword) error
	Close() error
}
