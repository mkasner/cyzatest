package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"

	"golang.org/x/net/context"

	"github.com/mkasner/cyza/internal/cyza"
	"github.com/mkasner/cyza/internal/model"
)

const (
	lAuthorizationUrl = "https://www.linkedin.com/oauth/v2/authorization"
	lAccessToken      = "https://www.linkedin.com/oauth/v2/accessToken"
	lBasicProfileUrl  = "https://api.linkedin.com/v1/people/~?format=json"
)

type accessToken struct {
	AccessToken      string `json:"access_token"`
	ExpiresIn        string `json:"expires_in"`
	Error            string `json:"error"`
	ErrorDescription string `json:"error_description"`
}
type lBasicProfile struct {
	Id        string `json:"id"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}

func (t *Server) liCallback(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	q := r.URL.Query()
	lerr := q.Get("error_description")
	if len(lerr) > 0 {
		http.Error(w, lerr, http.StatusInternalServerError)
		log.Println(lerr)
		return
	}
	code := q.Get("code")
	reqData := t.lAccessTokenRequest(code)
	fmt.Println(reqData)
	req, err := http.NewRequest(http.MethodPost, lAccessToken, strings.NewReader(reqData.Encode()))
	if err != nil {
		http.Error(w, lerr, http.StatusInternalServerError)
		log.Println(err)
		return
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		http.Error(w, lerr, http.StatusInternalServerError)
		log.Println(err)
		return
	}
	var at accessToken
	body, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		http.Error(w, lerr, http.StatusInternalServerError)
		log.Println(err)
		return
	}
	err = json.Unmarshal(body, &at)
	if err != nil {
		http.Error(w, lerr, http.StatusInternalServerError)
		log.Println(err)
		return
	}
	fmt.Println("accessTokenBody", string(body))
	fmt.Println("accessToken", at)
	if len(at.Error) > 0 {
		err := errors.New(at.ErrorDescription)
		http.Error(w, lerr, http.StatusInternalServerError)
		log.Println(err)
		return

	}
	if len(at.AccessToken) == 0 {
		err := errors.New("No access token received")
		http.Error(w, lerr, http.StatusInternalServerError)
		log.Println(err)
		return
	}
	// get basic profile info
	req, err = http.NewRequest(http.MethodGet, lBasicProfileUrl, nil)
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", at.AccessToken))
	resp, err = client.Do(req)
	if err != nil {
		http.Error(w, lerr, http.StatusInternalServerError)
		log.Println(err)
		return
	}
	var bp lBasicProfile
	body, err = ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		http.Error(w, lerr, http.StatusInternalServerError)
		log.Println(err)
		return
	}
	err = json.Unmarshal(body, &bp)
	if err != nil {
		http.Error(w, lerr, http.StatusInternalServerError)
		log.Println(err)
		return
	}
	fmt.Println("basicProfileBody", string(body))
	fmt.Println("basicProfile", bp)
	if len(bp.Id) == 0 {
		err := errors.New("No basic profile received")
		http.Error(w, lerr, http.StatusInternalServerError)
		log.Println(err)
		return
	}
	newUser := model.User{
		Username: bp.Id,
		Name:     fmt.Sprintf("%s %s", bp.FirstName, bp.LastName),
		Status:   model.UserStatusPartProfile,
	}
	user, err := t.cyza.Store.User(ctx, newUser.Username, "")
	if err != nil {
		password := randString(8)
		user, err = t.cyza.CreateUser(ctx, newUser, password, password)
		if err != nil {
			http.Error(w, lerr, http.StatusInternalServerError)
			log.Println(err)
			return
		}
	}
	fmt.Println(user)
	http.Redirect(w, r, fmt.Sprintf("/auth/linkedin/login?lid=%s", user.Username), http.StatusSeeOther)
}
func (t *Server) liLogin(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	userid := r.URL.Query().Get("lid")
	fmt.Println(userid)
	user, err := t.cyza.Store.User(ctx, userid, "")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Println(err)
		return
	}
	fmt.Println(user)
	c := t.newCookie(user.Username, user.Id, getIp(r), r.UserAgent())
	fmt.Println(c)
	http.SetCookie(w, c)
	//fmt.Fprintf(w, "Welcome")
	http.Redirect(w, r, "/", http.StatusSeeOther)

}

func (t *Server) generateLAuthorization() string {
	state := randString(16)
	u, _ := url.Parse(lAuthorizationUrl)
	query := u.Query()
	query.Set("response_type", "code")
	query.Set("client_id", t.lclient)
	query.Set("redirect_uri", fmt.Sprintf("%s%s", cyza.GetHost("", t.cyza.Addr), "/auth/linkedin"))
	query.Set("state", state)
	query.Set("scope", "r_basicprofile")
	u.RawQuery, _ = url.QueryUnescape(query.Encode())
	fmt.Println(u.String())
	return u.String()

}
func (t *Server) lAccessTokenRequest(code string) url.Values {
	query := make(url.Values)
	query.Set("grant_type", "authorization_code")
	query.Set("code", code)
	query.Set("client_id", t.lclient)
	query.Set("client_secret", t.lsecret)
	query.Set("redirect_uri", fmt.Sprintf("%s%s", cyza.GetHost("", t.cyza.Addr), "/auth/linkedin"))
	return query
}
