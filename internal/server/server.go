// package server manager frontend rendering
package server

import (
	"net/http"

	"github.com/mkasner/cyza/internal/cyza"
	"github.com/mkasner/cyza/internal/session"
	"golang.org/x/net/context"
)

type Server struct {
	ctx       context.Context
	cyza      *cyza.Cyza
	session   session.Store
	separator string
	tmplPath  string
	lclient   string
	lsecret   string
}

func New(c *cyza.Cyza, tmplPath string, session session.Store, lclient, lsecret string) (*Server, error) {
	s := &Server{
		ctx:       context.Background(),
		cyza:      c,
		session:   session,
		separator: random(),
		tmplPath:  tmplPath,
		lclient:   lclient,
		lsecret:   lsecret,
	}
	http.Handle("/", s.authenticated(http.HandlerFunc(s.home)))
	http.Handle("/profile", s.authenticated(http.HandlerFunc(s.profile)))
	http.HandleFunc("/login", s.login)
	http.HandleFunc("/signup", s.signup)
	http.HandleFunc("/logout", s.logout)
	http.HandleFunc("/forgotpassword", s.forgotpassword)
	http.HandleFunc("/recoverpassword", s.recoverpassword)
	http.HandleFunc("/auth/linkedin", s.liCallback)
	http.HandleFunc("/auth/linkedin/login", s.liLogin)
	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("assets/"))))

	return s, nil
}
