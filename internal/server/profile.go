package server

import (
	"fmt"
	"net/http"

	"golang.org/x/net/context"

	"github.com/mkasner/cyza/internal/model"
)

type Profile struct {
	Result string
	Error  string
	User   model.User
}

func (t *Server) profile(w http.ResponseWriter, r *http.Request) {
	s := t.s(r)
	ctx := context.Background()

	var view Profile
	var err error
	view.User, err = t.cyza.Store.User(ctx, "", s.UserId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer func() {
		outBuff := GetBuff()
		index, err := t.getTemplate("index.html", "profile.html", "mainmenu.html")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		data := struct {
			Title   string
			Content interface{}
		}{
			Title:   "Profile",
			Content: view,
		}
		err = index.ExecuteTemplate(outBuff, "index.html", data)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		fmt.Fprintf(w, outBuff.String())
		PutBuff(outBuff)
	}()
	if r.Method == http.MethodPost {
		ctx := context.Background()
		r.ParseForm()
		view.User.Email = r.Form.Get("email")
		view.User.Name = r.Form.Get("name")
		view.User.Address.Street = r.Form.Get("street")
		view.User.Address.City = r.Form.Get("city")
		view.User.Address.State = r.Form.Get("state")
		view.User.Address.Country = r.Form.Get("country")
		// validations

		if len(view.User.Username) < 2 {
			view.Error = "Username too short"
			return
		}
		if len(view.User.Name) < 2 {
			view.Error = "Name too short"
			return
		}
		if len(view.User.Name) > 100 {
			view.Error = "Name too long"
			return
		}
		if len(view.User.Address.Street) > 100 {
			view.Error = "Street too long"
			return
		}
		if len(view.User.Address.Country) > 100 {
			view.Error = "Country too long"
			return
		}
		view.User, err = t.cyza.Store.SaveUser(ctx, view.User, "")
		if err != nil {
			view.Error = err.Error()
			fmt.Println(err)
			return
		}

	}
}
