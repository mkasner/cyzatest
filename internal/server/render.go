package server

import (
	"html/template"
	"path/filepath"
)

func (t *Server) getTemplate(name ...string) (*template.Template, error) {
	filepaths := make([]string, len(name))
	for i, n := range name {
		filepaths[i] = filepath.Join(t.tmplPath, n)
	}
	return template.ParseFiles(filepaths...)
}
