package server

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	"golang.org/x/net/context"

	"log"

	"github.com/mkasner/cyza/internal/model"
)

type Login struct {
	Username       string
	Password       string
	LAuthorization string
	Error          string
}

type Signup struct {
	Name           string
	Username       string
	Password       string
	RepeatPassword string
	Error          string
	Message        string
}

type ForgotPassword struct {
	Email   string
	Error   string
	Message string
}

type RecoverPassword struct {
	Password       string
	RepeatPassword string
	Error          string
}

func (t *Server) login(w http.ResponseWriter, r *http.Request) {

	var l Login
	defer func() {
		outBuff := GetBuff()
		index, err := t.getTemplate("index.html", "login.html")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		l.LAuthorization = t.generateLAuthorization()
		data := struct {
			Title   string
			Content interface{}
		}{
			Title:   "Login",
			Content: l,
		}
		err = index.ExecuteTemplate(outBuff, "index.html", data)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		fmt.Fprintf(w, outBuff.String())
		PutBuff(outBuff)
	}()
	if r.Method == http.MethodPost {
		ctx := context.Background()
		r.ParseForm()
		l.Username = r.Form.Get("username")
		if len(l.Username) < 2 {
			l.Error = "Username too short"
		}
		l.Password = r.Form.Get("password")

		err := t.cyza.Authenticate(ctx, l.Username, l.Password)
		if err != nil {
			log.Println(err)
			l.Error = err.Error()
			return
		}
		fmt.Println("authenticated")
		user, err := t.cyza.Store.User(ctx, l.Username, "")
		if err != nil {
			l.Error = err.Error()
			return
		}
		switch user.Status {
		case model.UserStatusActive, model.UserStatusPartProfile:
			http.SetCookie(w, t.newCookie(l.Username, user.Id, getIp(r), r.UserAgent()))
			http.Redirect(w, r, "/", http.StatusSeeOther)
		default:
			err := errors.New("Unknown user status")
			l.Error = err.Error()
		}

	}
}

func (t *Server) signup(w http.ResponseWriter, r *http.Request) {
	var s Signup
	defer func() {
		outBuff := GetBuff()
		index, err := t.getTemplate("index.html", "signup.html")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		data := struct {
			Title   string
			Content interface{}
		}{
			Title:   "Signup",
			Content: s,
		}
		err = index.ExecuteTemplate(outBuff, "index.html", data)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		fmt.Fprintf(w, outBuff.String())
		PutBuff(outBuff)
	}()
	if r.Method == http.MethodPost {
		var err error
		ctx := context.Background()
		r.ParseForm()
		s.Username = r.Form.Get("username")
		s.Password = r.Form.Get("password")
		s.RepeatPassword = r.Form.Get("repeatpassword")
		user := model.User{
			Username: s.Username,
			Email:    s.Username,
			Status:   model.UserStatusPartProfile,
		}
		if len(user.Email) < 2 {
			s.Error = "Email not valid"
			return
		}
		if len(user.Email) > 100 {
			s.Error = "Email not valid"
			return
		}

		_, err = t.cyza.CreateUser(ctx, user, s.Password, s.RepeatPassword)
		if err != nil {
			log.Println(err)
			s.Error = err.Error()
		} else {
			http.Redirect(w, r, "/login", http.StatusSeeOther)
			return
		}
	}
}

func (t *Server) forgotpassword(w http.ResponseWriter, r *http.Request) {
	var fp ForgotPassword
	defer func() {
		outBuff := GetBuff()
		index, err := t.getTemplate("index.html", "forgotpassword.html")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		data := struct {
			Title   string
			Content interface{}
		}{
			Title:   "Signup",
			Content: fp,
		}
		err = index.ExecuteTemplate(outBuff, "index.html", data)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		fmt.Fprintf(w, outBuff.String())
		PutBuff(outBuff)
	}()
	if r.Method == http.MethodPost {
		r.ParseForm()
		fp.Email = r.Form.Get("email")
		ctx := context.Background()
		user, err := t.cyza.Store.User(ctx, fp.Email, "")
		if err != nil {
			log.Println(err)
			fp.Error = "Email doesn't exist"
		}
		if err == nil {
			err = t.cyza.ForgotPassword(ctx, user)
			if err != nil {
				fp.Error = err.Error()

			} else {
				fp.Message = "We've sent you an email which you can use to recover your password"

			}
		}

	}
}

func (t *Server) logout(w http.ResponseWriter, r *http.Request) {
	s, cookie, err := t.checkCookie(r)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}
	err = t.session.DeleteSession(s.Id)
	if err != nil {
		log.Println(err)
	}
	cookie.Expires = time.Time{}
	http.SetCookie(w, cookie)
	http.Redirect(w, r, "/login", http.StatusSeeOther)
}

func (t *Server) recoverpassword(w http.ResponseWriter, r *http.Request) {
	var token string
	query := r.URL.Query()
	token = query.Get("t")
	var rp RecoverPassword
	var redirect bool
	defer func() {
		if redirect {
			return
		}
		outBuff := GetBuff()
		index, err := t.getTemplate("index.html", "recoverpassword.html")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		data := struct {
			Title   string
			Content interface{}
		}{
			Title:   "Recover password",
			Content: rp,
		}
		err = index.ExecuteTemplate(outBuff, "index.html", data)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		fmt.Fprintf(w, outBuff.String())
		PutBuff(outBuff)
	}()
	if len(token) == 0 {
		rp.Error = "Token not provided"
		return
	}

	if r.Method == http.MethodPost {
		ctx := context.Background()
		user, err := t.cyza.UserRecoverPassword(ctx, token)
		if err != nil {
			log.Println(err)
			rp.Error = err.Error()
			return
		}
		r.ParseForm()
		form := r.Form
		newPass := form.Get("password")
		newPassConfirm := form.Get("passwordconfirm")
		err = t.cyza.ValidatePassword(newPass, newPassConfirm)
		if err != nil {
			log.Println(err)
			rp.Error = err.Error()
			return
		}
		err = t.cyza.ChangePassword(ctx, user, newPass)
		if err != nil {
			log.Println("ChangePassword", err)
			rp.Error = err.Error()
			return
		}
		err = t.cyza.UpdateRecoverPassword(ctx, token, model.RpStatusUsed)
		if err != nil {
			log.Println("UpdateRecoverPassword", err)
			rp.Error = err.Error()
			return
		}
		err = t.cyza.Store.UpdateUserStatus(ctx, user.Id, model.UserStatusActive)
		if err != nil {
			log.Println("UpdateUserStatus", err)
			rp.Error = err.Error()
			return
		}
		redirect = true
		http.Redirect(w, r, "/login", http.StatusSeeOther)
	}
}
