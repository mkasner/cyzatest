package server

import (
	"crypto/rand"
	"crypto/sha1"
	"errors"
	"fmt"
	"io"
	"log"
	"math/big"
	"net"
	"net/http"
	"strings"
	"time"

	"github.com/mkasner/cyza/internal/session"
)

var (
	ErrSessionNotValid = errors.New("Session not valid")
	ErrSessionNotFound = errors.New("Session not found")
)

const (
	cookieKey     = "cyza"
	minRandLength = 32 // min length for random string
	maxRandLength = 64 // max length for random string
)

func (t *Server) newCookie(username string, userId string, ip, userAgent string) *http.Cookie {
	s := &session.Session{
		Id:        random(),
		Username:  username,
		UserId:    userId,
		UpdatedAt: time.Now(),
	}
	err := t.session.SaveSession(s)
	if err != nil {
		log.Println(err)

	}
	c := &http.Cookie{
		Name:     cookieKey,
		Value:    t.generateCookie(s.Id, ip, userAgent),
		HttpOnly: true,
		Path:     "/",
	}
	return c
}

func (t *Server) generateCookie(sessionId string, ip, userAgent string) string {
	hash := t.generateHash(sessionId, ip, userAgent)
	return fmt.Sprintf("%s%s%s", sessionId, t.separator, hash)
}

func (t *Server) generateHash(sessionId, ip, userAgent string) string {
	h := sha1.New()
	io.WriteString(h, sessionId)
	io.WriteString(h, ip)
	io.WriteString(h, userAgent)
	return fmt.Sprintf("%x", h.Sum(nil))
}

func (t *Server) checkCookie(r *http.Request) (*session.Session, *http.Cookie, error) {
	var s *session.Session
	cookie, err := r.Cookie(cookieKey)
	if err != nil {
		return s, cookie, err
	}
	split := strings.Split(cookie.Value, t.separator)
	if len(split) != 2 {
		return s, cookie, ErrSessionNotValid
	}
	sessionId := split[0]
	//check hash
	providedHash := split[1]
	hash := t.generateHash(sessionId, getIp(r), r.UserAgent())
	if providedHash != hash {
		return s, cookie, ErrSessionNotValid
	}

	s, err = t.session.Session(sessionId)
	if err != nil {
		return s, cookie, err
	}
	if s == nil {
		return s, cookie, errors.New("Session not found")
	}

	return s, cookie, nil
}

// func s fetches session, it doesn't check for errors, because it's meant to be used after
// authentication is done
func (t *Server) s(r *http.Request) *session.Session {
	var s *session.Session
	cookie, err := r.Cookie(cookieKey)
	if err != nil {
		return s
	}
	split := strings.Split(cookie.Value, t.separator)
	if len(split) != 2 {
		return s
	}
	sessionId := split[0]
	s, _ = t.session.Session(sessionId)
	return s
}

// generates random string specified with minLength and maxLength
func random() string {
	return randString(randLength())
}

func randLength() int {
	rl, _ := rand.Int(rand.Reader, big.NewInt(maxRandLength-minRandLength))
	return int(rl.Int64()) + minRandLength
}

// returns random hex string specified by length
func randString(length int) string {
	buff := make([]byte, length/2)
	io.ReadFull(rand.Reader, buff)
	return fmt.Sprintf("%x", buff)
}

func (t *Server) authenticated(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _, err := t.checkCookie(r)
		if err != nil {
			http.Redirect(w, r, "/login", http.StatusSeeOther)
			return
		}
		next.ServeHTTP(w, r)
	})
}
func getIp(r *http.Request) string {
	if ipProxy := r.Header.Get("X-FORWARDED-FOR"); len(ipProxy) > 0 {
		return ipProxy
	}
	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	return ip
}
