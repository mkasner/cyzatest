package model

import "time"

type UserStatus int

const (
	UserStatusNew UserStatus = iota + 1
	UserStatusPartProfile
	UserStatusActive
	UserStatusDisabled
	UserStatusDeleted
)

type User struct {
	Id       string `bson:"_id,omitempty"`
	Username string
	Name     string
	Email    string
	Phone    string
	Address  Address
	Created  time.Time
	Status   UserStatus
}

type Address struct {
	Street  string
	City    string
	State   string
	Country string
}

type Password struct {
	Id       string `bson:"_id,omitempty"`
	Password string
}

type RecoverPasswordStatus int

const (
	RpStatusNew RecoverPasswordStatus = iota + 1
	RpStatusSent
	RpStatusUsed
	RpStatusExpired
	RpStatusCancelled
)

type RecoverPassword struct {
	Email   string
	UserId  string
	Token   string
	Status  RecoverPasswordStatus
	Expires time.Time
}
