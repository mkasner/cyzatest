const ExtractTextPlugin = require('extract-text-webpack-plugin')
module.exports = {
	entry: './src/js/main.js',
	output: {
		filename: 'js/app.js',
		path: '../assets/'
	},

	module: {
		loaders: [{
			test: /\.js$/,
			exclude: /node_modules/,
			loader: 'babel?presets[]=es2015'
		}]
	},
	devtool: 'source-map',
	plugins: [
		new ExtractTextPlugin('bundle.css')
	]

}
