
install: main.go
	go install -v 

.PHONY: clean
clean:
	go clean -x

imports:
	go list github.com/mkasner/cyza | xargs -n 1 deplist | grep -v github.com/mkasner/cyza | sort -u

stop:
	kill `pidof cyza`

