package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/mkasner/cyza/internal/service"
	"github.com/mkasner/cyza/internal/session/inmem"
	"github.com/mkasner/cyza/internal/store"
)

func main() {
	var (
		addr      string
		database  string
		templates string
		// mail settings
		mail     string
		port     int
		username string
		password string
		lclient  string
		lsecret  string
	)

	flag.StringVar(&addr, "addr", ":8088", "http addr")
	flag.StringVar(&database, "db", "localhost:27017", "db url")
	flag.StringVar(&templates, "templates", "templates", "templates path")
	flag.StringVar(&mail, "mail", "localhost", "mail server")
	flag.IntVar(&port, "port", 1025, "mail port")
	flag.StringVar(&username, "user", "a", "mail user")
	flag.StringVar(&password, "pass", "a", "mail password")
	flag.StringVar(&lclient, "lclient", "", "linked in client")
	flag.StringVar(&lsecret, "lsecret", "", "linked in secret")

	flag.Parse()
	fmt.Println("Initializing database....")
	var err error
	store, err := store.NewMongoStore(database)
	if err != nil {
		panic(err)
	}
	sessions, err := inmem.NewInmemSessionStore()
	if err != nil {
		panic(err)
	}
	fmt.Println("Initializing sessions....")

	srv, err := service.New(addr, store, templates, sessions, mail, port, username, password, lclient, lsecret)
	if err != nil {
		panic(err)
	}
	defer func() {
		store.Close()
		sessions.Close()
	}()
	var services []*srvc
	services = add(services, srv)
	wait(services)
}

// service management
type srvc struct {
	goer       goer
	stop, done chan struct{}
}

type goer interface {
	Go(chan struct{}, chan struct{})
}

func add(services []*srvc, goer goer) []*srvc {
	services = append(services, &srvc{goer: goer, stop: make(chan struct{}), done: make(chan struct{})})
	return services
}

func wait(services []*srvc) {
	start(services)
	c := make(chan os.Signal, 2)
	signal.Notify(c, syscall.SIGHUP, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGABRT, syscall.SIGTERM, syscall.SIGUSR1, syscall.SIGUSR2)
	for {
		select {
		case sig := <-c:
			switch sig.(syscall.Signal) {
			case syscall.SIGINT, syscall.SIGQUIT, syscall.SIGTERM:
				stop(services)
				return
			case syscall.SIGABRT:
				os.Exit(1)
			}
		}
	}
}

func start(services []*srvc) {
	for _, s := range services {
		s.goer.Go(s.stop, s.done)
	}

}

func stop(services []*srvc) {
	for _, s := range services {
		s.stop <- struct{}{}
		<-s.done
	}

}
